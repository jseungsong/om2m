About OM2M
==========


OM2M implements oneM2M standard. It provides a horizontal Service Common Entity (CSE) that can be deployed in an M2M/IoT server, a gateway, or a device. Each CSE provides Application Enablement, Security, Triggering, Notification, Persistency, Device Interworking, Device Management, etc.
OM2M exposes a RESTful API providing primitive procedures for machines authentication, resources discovery, applications registration, containers management, synchronous and asynchronous communications, access rights authorization, groups organisation, and re-targeting.
OM2M is a Java implementation running on top of an OSGi Equinox runtime, making it highly extensible via plugins. It is built as an Eclipse product using Maven and Tycho. Each plugin offers specific functionalities, and can be remotely installed, started, stopped, updated, and uninstalled without requiring a reboot.

Prerequisites:
==========

* JAVA 1.7 or 1.8 is required to run OM2M.
* Apache Maven 3 or later is required to build OM2M.


Building OM2M from sources using maven:
=================================

* Clone the OM2M project : git@gitlab.eclipse.org:eclipse/om2m/om2m.git with ssh, https://gitlab.eclipse.org/eclipse/om2m/om2m.git with https
* Go to the parent project directory "org.eclipse.om2m"
* Build the OM2M project using the following command (An internet connection is required to download required dependencies):

 > mvn clean install  -DskipTests=true
 

* Two Eclipse products will be generated after a successful built:

  * The IN product can be found on this directory: om2m/org.eclipse.om2m/org.eclipse.om2m.site.in-cse/target/products/in-cse/"os"/"ws"/"arch"
  * The MN product can be found on this directory: om2m/org.eclipse.om2m/org.eclipse.om2m.site.mn-cse/target/products/mn-cse/"os"/"ws"/"arch"
  * The ASN product can be found on this directory: om2m/org.eclipse.om2m/org.eclipse.om2m.site.asn-cse/target/products/asn-cse/"os"/"ws"/"arch"


Test OM2M:
=========

* Go to the IN-CSE product directory: om2m/org.eclipse.om2m/org.eclipse.om2m.site.in-cse/target/products/in-cse/"os"/"ws"/"arch".
* Start the IN-CSE by executing the "start.bat" script on Windows or "start.sh" on Linux and Mac OS.
* WARNING for Windows users: don't double click on the "start.bat" file from Eclipse explorer on Windows. This cause unwanted loading of bundles and the platform will NOT be launched. Use the File Explorer instead.
* Once the IN-CSE is started, you will see an OSGi console. You can type “ss” to report a summary status of all installed bundles. Type "exit" to shutdown.

WEB interface:
==============

* Open your browser and connect to the address "http://127.0.0.1:8080/webpage" to access the IN-CSE web interface.
* Enter username "admin" and password "admin" then click on login button to display the IN-CSE resource tree.

Congratulation!! You built and started OM2M successfully.